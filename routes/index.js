
/*
 * GET home page.
 */
var fs = require('fs'),
    path = require('path'),
    marked = require('marked');

marked.setOptions({
  gfm: true,
  pedantic: false,
  sanitize: false
});

var metaRE = /^[ \t]*\@(.+?)(?:\((.+)\))?[ \t]*$/gm;
// var metaRE = /^@(.*)\((.*)\)$/m;

/* An optionally indented line that
    starts with @
    followed by characters (tag value, $1)
    optionally followed by characters in parentheses (tag class, $2)
*/

// Return all pattern matches with captured groups
RegExp.prototype.execAll = function(string) {
    var match = null;
    var matches = new Array();
    while (match = this.exec(string)) {
        var matchArray = [];
        for (i in match) {
            if (parseInt(i) == i) {
                matchArray.push(match[i]);
            }
        }
        matches.push(matchArray);
    }
    return matches;
}

exports.index = function(req, res){
  res.render('index.html', { 
    title: 'Micro CMS',
    content: 'nieźle!'
  })
};


exports.html = function(req, res, next){
  filename = path.normalize(__dirname + '/../content/' + req.params[0]);
  console.log ('Attempting to read HTML file %s', filename);
  path.exists(filename, function(exists) {
    if (exists)
      fs.readFile(filename, 'utf-8', function(err, data) {
        console.log('HTML file red. Responding as is.')
        if (err) throw (err);
        res.end(data);
        console.log('Done :)');
      });
    else next();
  });
}


exports.markdown = function(req, res, next){
  filename = path.normalize(__dirname + '/../content/' + req.params[0]);
  console.log ('Attempting to read Markdown file %s', filename);
  path.exists(filename, function(exists) {
    if (exists) {
      /* To do:
      + Read file
      * if file is markdown
        + Store contents as raw
        + Extract meta (store as meta) and remove meta tags (store rest as markdown)
        * Split into sections by headings ?
        * Convert markdown and store as html
      */
      fs.readFile(filename, 'utf-8', function(err, data) {
        console.log('Markdown file red. Preparing response.')
        if (err) throw (err);

        var raw = data,
            meta = {},
            markdown = '',
            html = '';

        console.log('Doing meta');
        while (match = metaRE.exec(raw)) {
          console.log('> ' + match[0]);
          /* To do: list of classes separated by colon eg. @Tadeusz (author, publisher, awesome person) */
          tagClass = match[2] ? match[2].trim().toLowerCase() : 'tag';
          tagValue = match[1].trim();

          if (!meta[tagClass]) meta[tagClass] = [];
          o = {value: tagValue};
          o[tagClass] = tagValue; // for convenience
          meta[tagClass].push(o);
        }
        console.log(meta);

        markdown = raw.replace(metaRE, '');
        html = marked(markdown);



        res.render('content.html', { 
          title: 'Micro CMS Conent',
          content: html,
          meta: meta
        });
        console.log('Done :)');
      });
    }
    else next();
  });
};