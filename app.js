
/**
 * Module dependencies.
 */

var express = require('express'),
    routes = require('./routes'),
    hbs = require('hbs')

var app = module.exports = express.createServer();

// Configuration

app.configure(function(){
  app.set('views', __dirname + '/templates');
  app.set('view engine', 'hbs');
  app.set('view options', {
    layout: false
  });
  app.register(".html", hbs);
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(__dirname + '/templates/static'));
});

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.configure('production', function(){
  app.use(express.errorHandler());
});

// Routes

app.get('/', routes.index);
app.get(/(.+\.html)/, routes.html);
app.get(/(.+\.md)/, routes.markdown);
app.get(/(.+\.txt)/, routes.text);


app.listen(8000, function(){
  console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);
});
