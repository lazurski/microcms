/**
* This is our Node.JS code, running server-side.
*/
var 
  sys = require('util'),
  http = require('http'),
  port = 8000; // TODO: set your actual port here

  http.createServer(function(request, response) {
    response.writeHead(200, {
        'Content-Type': 'text/html'
    });

/**
 * Here you can route or process the page based on request.url,
 * or you may want to use a module such as node-router.
 */
 console.log('Request for ' + request.url)
 response.end("Node here!");
}).listen(port);

console.log('Server listening on port ' + port);